package org.example.config;

import org.example.logging.TransactionLoggingAspect;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextListener;

@Configuration
public class ApplicationConfig {
    @Bean
    public TransactionLoggingAspect transactionLoggingAspect() {
        return new TransactionLoggingAspect();
    }

    @Bean
    public ServletListenerRegistrationBean<RequestContextListener> requestContextListener() {
        return new ServletListenerRegistrationBean<>(new RequestContextListener());
    }
}
