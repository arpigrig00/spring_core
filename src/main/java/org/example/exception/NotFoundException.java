package org.example.exception;

public class  NotFoundException extends RuntimeException{
    private static final String NOT_FOUND = "Entity not found";

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(){
        super(NOT_FOUND);
    }
}
