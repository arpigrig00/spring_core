package org.example.exception;

public class UnAuthenticatedException extends RuntimeException{
    private static final String UNAUTHENTICATED = "Unauthenticated user.";

    public UnAuthenticatedException(String message) {
        super(message);
    }

    public UnAuthenticatedException(){
        super(UNAUTHENTICATED);
    }
}
