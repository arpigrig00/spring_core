package org.example.exception;

import javax.security.sasl.AuthenticationException;

public class BlockedUserException extends RuntimeException {
    private static final String BLOCKED = "User with given credentials are blocked.";

    public BlockedUserException(String message) {
        super(message);
    }

    public BlockedUserException(){
        super(BLOCKED);
    }
}
