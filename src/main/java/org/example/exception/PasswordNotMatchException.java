package org.example.exception;

public class PasswordNotMatchException extends RuntimeException{
    private static final String NOT_MATCH = "Passwords doesn't match";

    public PasswordNotMatchException(String message) {
        super(message);
    }

    public PasswordNotMatchException(){
        super(NOT_MATCH);
    }
}
