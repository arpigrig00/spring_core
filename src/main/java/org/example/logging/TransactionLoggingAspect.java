package org.example.logging;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Aspect
@Component
public class TransactionLoggingAspect {

    @Pointcut("execution(* org.example.controller..*(..))")
    public void applicationPackagePointcut() {
    }

    @Before("applicationPackagePointcut()")
    public void addTransactionId() {
        String transactionId = UUID.randomUUID().toString();
        MDC.put("transactionId", transactionId);
        System.out.println("Transaction started with ID: " + transactionId);
    }

    @AfterReturning("applicationPackagePointcut()")
    public void afterReturning() {
        System.out.println("Transaction completed successfully with ID: " + MDC.get("transactionId"));
        MDC.remove("transactionId");
    }

    @AfterThrowing("applicationPackagePointcut()")
    public void afterThrowing() {
        System.out.println("Transaction failed with ID: " + MDC.get("transactionId"));
        MDC.remove("transactionId");
    }
}
