package org.example.logging;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.UUID;

@Component
public class LoggingInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String transactionId = MDC.get("transactionId");
        if (transactionId == null) {
            transactionId = UUID.randomUUID().toString();
            MDC.put("transactionId", transactionId);
        }

        System.out.println("Request URL: " + request.getRequestURL());
        System.out.println("Request Method: " + request.getMethod());
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        System.out.println("Response Status: " + response.getStatus());
        if (ex != null) {
            System.out.println("Exception: " + ex.getMessage());
        }
        MDC.remove("transactionId");
    }
}
