package org.example.controller;

import java.time.LocalDate;
import java.util.List;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.example.dto.request.TraineeCreateRequest;
import org.example.dto.request.TraineeTrainersUpdateRequest;
import org.example.dto.request.TraineeUpdateRequest;
import org.example.dto.response.TraineeResponse;
import org.example.dto.response.TraineeTrainerResponse;
import org.example.dto.response.TraineeTrainingsResponse;
import org.example.service.TraineeService;

import org.example.dto.response.MemberCreateResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trainee")
@RequiredArgsConstructor
public class TraineeController {
    private final TraineeService traineeService;

    @PostMapping
    public ResponseEntity<MemberCreateResponse> create(@Valid @RequestBody TraineeCreateRequest request) {
        MemberCreateResponse response = traineeService.createTrainee(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TraineeResponse> get(@PathVariable("id") Long id) {
        TraineeResponse trainee = traineeService.get(id);
        return ResponseEntity.ok(trainee);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TraineeResponse> update(@PathVariable("id") Long id,
                                                  @Valid @RequestBody TraineeUpdateRequest updateRequest) {
        TraineeResponse response = traineeService.updateTrainee(id, updateRequest);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}/unrelated-trainers")
    public ResponseEntity<List<TraineeTrainerResponse>> update(
        @PathVariable("id") Long id) {
        List<TraineeTrainerResponse> nonRelatedTrainers = traineeService.getUnrelatedTrainers(id);
        return ResponseEntity.ok(nonRelatedTrainers);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> remove(@PathVariable("id") Long id) {
        traineeService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/trainings")
    public ResponseEntity<List<TraineeTrainingsResponse>> getTrainings(
        @RequestParam(value = "traineeUsername") String traineeUsername,
        @RequestParam(value = "trainerUsername", required = false) String trainerUsername,
        @RequestParam(value = "from", required = false) LocalDate from,
        @RequestParam(value = "to", required = false) LocalDate to) {
        List<TraineeTrainingsResponse> trainings =
            traineeService.getTrainings(traineeUsername, trainerUsername, from, to);
        return ResponseEntity.ok(trainings);
    }

    @PatchMapping("/{id}/activate/{isActive}")
    public ResponseEntity<Void> changeStatus(@PathVariable("id") Long id, @PathVariable("isActive") Boolean isActive) {
        traineeService.updateIsActive(id, isActive);
        return ResponseEntity.status(HttpStatus.OK).build();

    }

    @PutMapping("/{id}/change-trainers")
    public ResponseEntity<List<TraineeTrainerResponse>> changeTrainers(@PathVariable("id") Long id, @Valid @RequestBody
                                                                 TraineeTrainersUpdateRequest updateRequest) {
        List<TraineeTrainerResponse> newTrainers = traineeService.updateTrainings(id, updateRequest.trainersUsername());
        return ResponseEntity.ok(newTrainers);
    }
}
