package org.example.controller;

import java.time.LocalDate;
import java.util.List;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.example.dto.request.TrainerCreateRequest;
import org.example.dto.request.TrainerUpdateRequest;
import org.example.dto.response.MemberCreateResponse;
import org.example.dto.response.TrainerResponse;
import org.example.dto.response.TrainerTrainingsResponse;
import org.example.service.TrainerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trainer")
@RequiredArgsConstructor
public class TrainerController {
    private final TrainerService trainerService;

    @PostMapping
    public ResponseEntity<MemberCreateResponse> create(@Valid @RequestBody TrainerCreateRequest createRequest) {
        MemberCreateResponse trainer = trainerService.createTrainer(createRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(trainer);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TrainerResponse> get(@PathVariable("id") String username) {
        TrainerResponse trainer = trainerService.get(username);
        return ResponseEntity.ok(trainer);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TrainerResponse> update(@PathVariable("id") Long id, @Valid @RequestBody TrainerUpdateRequest updateRequest) {
        TrainerResponse response = trainerService.updateTrainer(id, updateRequest);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/trainings")
    public ResponseEntity<List<TrainerTrainingsResponse>> getTrainings(
        @RequestParam(value = "trainerUsername") String trainerUsername,
        @RequestParam(value = "traineeUsername", required = false) String traineeUsername,
        @RequestParam(value = "from", required = false) LocalDate from,
        @RequestParam(value = "to", required = false) LocalDate to) {
        List<TrainerTrainingsResponse> training =
            trainerService.getTrainings(trainerUsername, traineeUsername, from, to);
        return ResponseEntity.ok(training);
    }

    @PatchMapping("/{id}/activate/{isActive}")
    public ResponseEntity<Void> changeStatus(@PathVariable("id") Long id, @PathVariable("isActive") Boolean isActive) {
        trainerService.updateIsActive(id, isActive);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
