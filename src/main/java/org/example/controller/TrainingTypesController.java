package org.example.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.example.dto.response.TrainingTypeResponse;
import org.example.service.TrainingTypeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/training-types")
public class TrainingTypesController {

    private final TrainingTypeService trainingTypeService;

    @GetMapping
    ResponseEntity<List<TrainingTypeResponse>> getAll() {
        List<TrainingTypeResponse> type = trainingTypeService.getAll();
        return ResponseEntity.ok(type);
    }
}
