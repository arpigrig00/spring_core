package org.example.dto.response;

import lombok.Builder;

@Builder
public record TrainingTypeResponse(
    Long id,
    String trainingType
) {
}
