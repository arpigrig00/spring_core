package org.example.dto.response;

import java.util.List;

import lombok.Builder;

@Builder
public record TrainerResponse(
    Long id,
    String username,
    String firstName,
    String lastName,
    TrainingTypeResponse specialization,
    Boolean isActive,
    List<TrainerTraineeResponse> trainees
) {
}
