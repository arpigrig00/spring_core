package org.example.dto.response;

import lombok.Builder;

@Builder
public record TrainerTraineeResponse(
    String username,
    String firstName,
    String lastName
) {
}
