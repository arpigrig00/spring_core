package org.example.dto.response;

import lombok.Builder;

@Builder
public record MemberCreateResponse(
    String username,
    String password
) {
}
