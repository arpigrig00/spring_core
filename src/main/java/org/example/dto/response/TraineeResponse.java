package org.example.dto.response;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;

@Builder
public record TraineeResponse(
    Long id,
    String username,
    String firstName,
    String lastName,
    @JsonFormat(pattern = "yyyy-MM-dd") LocalDate dob,
    String address,
    Boolean isActive,
    List<TraineeTrainerResponse> trainers
) { }
