package org.example.dto.response;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;

@Builder
public record TrainerTrainingsResponse(
    Long id,
    String trainingName,
    @JsonFormat(pattern = "yyyy-MM-dd") LocalDate trainingDate,
    TrainingTypeResponse trainingType,
    Long trainingDuration,
    String traineeUsername
) {
}
