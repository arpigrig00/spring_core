package org.example.dto.response;

import lombok.Builder;

@Builder
public record TraineeTrainerResponse(
    String username,
    String firstName,
    String lastName,
    TrainingTypeResponse specialization
) { }
