package org.example.dto.request;

import lombok.Builder;

@Builder
public record TrainerUpdateRequest(Boolean isActive, Long trainingTypeId, String firstName, String lastName) {
}
