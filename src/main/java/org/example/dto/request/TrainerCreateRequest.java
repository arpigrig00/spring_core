package org.example.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import org.example.annotation.Password;

@Builder
public record TrainerCreateRequest(String firstName, String lastName, Long trainingTypeId,
                                   @NotNull @Password String password
) {
}
