package org.example.dto.request;

import java.time.LocalDate;

import org.example.enums.ActionType;
import lombok.Builder;
import lombok.Getter;

@Builder
public record TrainerWorkloadRequest(
    String trainerUsername,
    String  trainerFirstname,
    String  trainerLastname,
    Boolean isActive,
    LocalDate trainingDate,
    Long trainingDuration,
    ActionType actionType
) {}
