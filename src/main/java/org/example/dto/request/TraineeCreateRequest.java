package org.example.dto.request;

import java.time.LocalDate;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import org.example.annotation.Password;

@Builder
public record TraineeCreateRequest(
    @NotNull String firstName,
    @NotNull String lastName,
    LocalDate dob,
    String address,
    @NotNull @Password String password
    ) {
}
