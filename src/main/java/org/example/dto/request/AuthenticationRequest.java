package org.example.dto.request;

import jakarta.validation.constraints.Email;
import lombok.Builder;
import org.example.annotation.Password;

@Builder
public record AuthenticationRequest(
        @Email(message = "Please provide a valid email")
        String username,

        @Password
        String password
) { }
