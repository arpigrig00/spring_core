package org.example.dto.request;

import java.time.LocalDate;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
public record TraineeUpdateRequest(
    @NotNull String firstName,
    @NotNull String lastName ,
    @NotNull Boolean isActive,
    LocalDate dob,
    String address)
{ }
