package org.example.dto.request;

import java.time.LocalDate;

import lombok.Builder;

@Builder
public record TrainingCreateRequest(String trainerUsername, String traineeUsername, String trainingName, LocalDate date, Long duration) {
}
