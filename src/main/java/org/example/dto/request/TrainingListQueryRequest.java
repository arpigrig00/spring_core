package org.example.dto.request;

import java.time.LocalDate;

public record TrainingListQueryRequest(String trainerUsername, String traineeUsername, LocalDate from , LocalDate to)
{ }
