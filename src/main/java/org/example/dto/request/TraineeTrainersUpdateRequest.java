package org.example.dto.request;

import java.util.List;

import jakarta.validation.constraints.NotNull;

public record TraineeTrainersUpdateRequest(@NotNull List<String> trainersUsername) {
}
