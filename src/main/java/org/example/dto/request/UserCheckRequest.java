package org.example.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
public record UserCheckRequest(
    @NotNull String username,
    @NotNull String password
) {
}
