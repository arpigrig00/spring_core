package org.example.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
public record PasswordChangeRequest(
    @NotNull
    String username,
    @NotNull
    String oldPassword,
    @NotNull
    String newPassword
) {
}
