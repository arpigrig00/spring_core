package org.example.repository;

import java.util.Optional;

import org.example.domain.Trainee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TraineeRepository extends JpaRepository<Trainee, Long> {
    Optional<Trainee> findByUser_Username(String username);
}
