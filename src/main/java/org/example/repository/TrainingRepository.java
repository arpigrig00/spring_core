package org.example.repository;

import java.util.List;

import org.example.domain.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TrainingRepository extends JpaRepository<Training, Long>, JpaSpecificationExecutor<Training> {
    @Query("Select t from Training t where  t.trainee.user.username = :traineeUsername" )
    List<Training> findAllByTraineeUsername(@Param("traineeUsername") String traineeUsername);
}
