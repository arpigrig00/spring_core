package org.example.repository;

import java.util.Optional;

import org.example.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    @Query("select count(u.id) from User  u where u.username LIKE :username%")
    int findCountByUsername(@Param("username") String username);

    boolean existsByUsernameAndPassword(String username, String password);
}
