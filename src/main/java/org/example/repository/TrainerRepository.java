package org.example.repository;

import java.util.List;
import java.util.Optional;

import org.example.domain.Trainer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainerRepository extends JpaRepository<Trainer, Long> {
    Optional<Trainer> findByUser_Username(String username);

    List<Trainer> findAllByIdNotIn(List<Long> ids);

    List<Trainer> findAllByUser_UsernameIsIn(List<String> user_usernameList);

}
