package org.example.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.example.annotation.Password;

public class PasswordValidator implements ConstraintValidator<Password,String> {
    @Override
    public boolean isValid(String password, ConstraintValidatorContext constraintValidatorContext) {
        return password != null
            && password.length() >= 12
            && password.matches("\\A\\p{ASCII}*\\z")
            && password.matches(".*\\d.*")
            && password.matches(".*[!@#$%^&*()\\-_.<>/\\[]*].*")
            && password.matches(".*[A-Z].*")
            && password.matches(".*[a-z].*");
    }
}
