package org.example.specification;

import java.time.LocalDate;

import jakarta.persistence.criteria.Join;
import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.springframework.data.jpa.domain.Specification;

public class TrainingSpecification {

    static String TRAINER = "trainer";
    static String TRAINEE = "trainee";
    static String USER = "user";
    static String USERNAME = "username";
    static String TRAINING_DATE = "trainingDate";

    public static Specification<Training> hasTrainerUsername(String trainerUsername) {
        return (root, query, builder) -> {
            if (trainerUsername == null) {
                return null;
            }
            Join<Training, Trainer> trainer = root.join(TRAINER);
            return builder.equal(trainer.get(USER).get(USERNAME), trainerUsername);
        };
    }

    public static Specification<Training> hasTraineeUsername(String traineeUsername) {
        return (root, query, builder) -> {
            if (traineeUsername == null) {
                return null;
            }
            Join<Training, Trainee> trainee = root.join(TRAINEE);

            return builder.equal(trainee.get(USER).get(USERNAME), traineeUsername);
        };
    }

    public static Specification<Training> hasFromAfter(LocalDate from) {
        return (root, query, builder) -> from == null ? null : builder.greaterThanOrEqualTo(root.get(TRAINING_DATE), from);
    }

    public static Specification<Training> hasToBefore(LocalDate to) {
        return (root, query, builder) -> to == null ? null : builder.lessThanOrEqualTo(root.get(TRAINING_DATE), to);
    }
}
