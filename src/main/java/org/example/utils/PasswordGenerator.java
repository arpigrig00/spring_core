package org.example.utils;

import java.security.SecureRandom;
public final class PasswordGenerator {
    public static final String CHAR_POOL = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final SecureRandom RANDOM = new SecureRandom();

    private PasswordGenerator(){
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");

    }
    public static String generateRandomString(int length) {
        StringBuilder randomString = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            int randomIndex = RANDOM.nextInt(CHAR_POOL.length());
            randomString.append(CHAR_POOL.charAt(randomIndex));
        }

        return randomString.toString();
    }

}
