package org.example.service;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.example.domain.TrainingType;
import org.example.dto.response.TrainingTypeResponse;
import org.example.repository.TrainingTypeRepository;
import org.springframework.stereotype.Service;

import org.example.exception.NotFoundException;
@Service
@RequiredArgsConstructor
public class TrainingTypeService {

    private final TrainingTypeRepository trainingTypeRepository;

    public TrainingType get(Long id) {
        return trainingTypeRepository.findById(id).orElseThrow(() -> new NotFoundException("Training type with given " + id+ " doesn't exist"));
    }

    public List<TrainingTypeResponse> getAll() {
        return trainingTypeRepository.findAll().stream().map(Mapper::mapTrainingTypeToTrainingTypeResponse).toList();

    }

    public TrainingType create(String type) {
        return trainingTypeRepository.save(TrainingType.builder().trainingTypeName(type).build());
    }
}
