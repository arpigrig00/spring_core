package org.example.service;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.example.domain.Training;
import org.example.dto.request.TrainerWorkloadRequest;
import org.example.enums.ActionType;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TrainingWorkloadService {
    private final JmsTemplate jmsTemplate;
    private static final String REPORT_QUEUE = "workload-service";

    //public void sendBulkWorkloadRequest(List<TrainerWorkloadRequest> trainerWorkloadRequest) {
    //    jmsTemplate.convertAndSend(REPORT_QUEUE, trainerWorkloadRequest);
    //
    //}
    public void sendWorkloadRequest(Training training, ActionType type) {
        TrainerWorkloadRequest request = Mapper.mapTrainingToTrainerWorkloadRequest(training, type);
        jmsTemplate.convertAndSend(REPORT_QUEUE, request);

    }
}
