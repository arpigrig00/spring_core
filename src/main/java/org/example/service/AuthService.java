package org.example.service;

import java.util.HashMap;
import java.util.Map;

import lombok.RequiredArgsConstructor;
import org.example.domain.User;
import org.example.dto.request.AuthenticationRequest;
import org.example.dto.request.UserCheckRequest;
import org.example.dto.response.AuthenticationResponse;
import org.example.exception.BlockedUserException;
import org.example.exception.NotFoundException;
import org.example.exception.UnAuthenticatedException;
import org.example.repository.UserRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final UserRepository userRepository;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final LoginAttemptService loginAttemptService;

    public void checkAuthUser(UserCheckRequest checkRequest) {
        boolean exist = userRepository.existsByUsernameAndPassword(checkRequest.username(), checkRequest.password());
        if (!exist) {
            throw new UnAuthenticatedException();
        }
    }

    public AuthenticationResponse login(AuthenticationRequest request) {
        try {
            if (loginAttemptService.isBlocked()) {
                throw new BlockedUserException();
            }

            Authentication
                authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.username(), request.password()));
            User user = userRepository.findByUsername(authentication.getName()).orElseThrow(NotFoundException::new);
            String token = jwtService.generateToken(user);
            return AuthenticationResponse
                .builder()
                .token(token)
                .build();

        }  catch (BadCredentialsException e){
            System.out.println(e.getMessage());
            throw new BadCredentialsException("Invalid username or password");
        }
    }

    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public User getAuthUser() {
        return (User) getAuthentication().getPrincipal();
    }
}
