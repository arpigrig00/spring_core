package org.example.service;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.TrainingType;
import org.example.dto.request.TrainerWorkloadRequest;
import org.example.dto.response.MemberCreateResponse;
import org.example.dto.response.TraineeResponse;
import org.example.dto.response.TraineeTrainerResponse;
import org.example.dto.response.TraineeTrainingsResponse;
import org.example.dto.response.TrainerResponse;
import org.example.dto.response.TrainerTraineeResponse;
import org.example.dto.response.TrainerTrainingsResponse;
import org.example.dto.response.TrainingTypeResponse;
import org.example.enums.ActionType;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Mapper {

    public static TraineeResponse mapTraineeToTraineeResponse(Trainee trainee) {
        return TraineeResponse.builder()
            .id(trainee.getId())
            .username(trainee.getUser().getUsername())
            .address(trainee.getAddress())
            .dob(trainee.getDob())
            .isActive(trainee.getUser().getIsActive())
            .firstName(trainee.getUser().getFirstName())
            .lastName(trainee.getUser().getLastName())
            .trainers(trainee.getTrainers().stream().map(Mapper::mapTrainerToTraineeTrainerResponse).toList())
            .build();
    }

    public static TrainingTypeResponse mapTrainingTypeToTrainingTypeResponse(TrainingType trainingType) {
        return TrainingTypeResponse.builder()
            .id(trainingType.getId())
            .trainingType(trainingType.getTrainingTypeName()).build();
    }

    public static TraineeTrainerResponse mapTrainerToTraineeTrainerResponse(Trainer trainer) {
        return TraineeTrainerResponse
            .builder()
            .username(trainer.getUser().getUsername())
            .firstName(trainer.getUser().getFirstName())
            .lastName(trainer.getUser().getLastName())
            .specialization(mapTrainingTypeToTrainingTypeResponse(trainer.getTrainingType()))
            .build();
    }

    public static MemberCreateResponse mapMemberResponse(String username, String password) {
        return MemberCreateResponse.builder()
            .username(username)
            .password(password)
            .build();
    }

    public static TrainerResponse mapTrainerToTrainerResponse(Trainer trainer) {
        return TrainerResponse
            .builder()
            .id(trainer.getId())
            .username(trainer.getUser().getUsername())
            .firstName(trainer.getUser().getFirstName())
            .lastName(trainer.getUser().getLastName())
            .isActive(trainer.getUser().getIsActive())
            .specialization(mapTrainingTypeToTrainingTypeResponse(trainer.getTrainingType()))
            .trainees(trainer.getTrainees().stream().map(Mapper::mapTraineeToTrainerTraineeResponse).toList())
            .build();
    }

    public static TrainerTraineeResponse mapTraineeToTrainerTraineeResponse(Trainee trainee) {
        return TrainerTraineeResponse
            .builder()
            .firstName(trainee.getUser().getFirstName())
            .lastName(trainee.getUser().getLastName())
            .username(trainee.getUser().getUsername())
            .build();
    }

    public static TraineeTrainingsResponse mapTrainingToTraineeTrainingResponse(Training training) {
        return TraineeTrainingsResponse
            .builder()
            .id(training.getId())
            .trainerUsername(training.getTrainer().getUser().getUsername())
            .trainingName(training.getTrainingName())
            .trainingDate(training.getTrainingDate())
            .trainingDuration(training.getTrainingDuration())
            .trainingType(mapTrainingTypeToTrainingTypeResponse(training.getTrainer().getTrainingType()))
            .build();
    }

    public static TrainerTrainingsResponse mapTrainingToTrainerTrainingResponse(Training training) {
        return TrainerTrainingsResponse
            .builder()
            .id(training.getId())
            .traineeUsername(training.getTrainee().getUser().getUsername())
            .trainingName(training.getTrainingName())
            .trainingDate(training.getTrainingDate())
            .trainingDuration(training.getTrainingDuration())
            .trainingType(mapTrainingTypeToTrainingTypeResponse(training.getTrainer().getTrainingType()))
            .build();
    }

    public static TrainerWorkloadRequest mapTrainingToTrainerWorkloadRequest(Training training, ActionType type) {
        return TrainerWorkloadRequest.builder()
            .actionType(type)
            .trainerFirstname(training.getTrainer().getUser().getFirstName())
            .trainerLastname(training.getTrainer().getUser().getLastName())
            .isActive(training.getTrainer().getUser().getIsActive())
            .trainerUsername(training.getTrainer().getUser().getUsername())
            .trainingDuration(training.getTrainingDuration())
            .trainingDate(training.getTrainingDate())
            .build();
    }
}
