package org.example.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.TrainingType;
import org.example.domain.User;
import org.example.dto.request.TrainerCreateRequest;
import org.example.dto.request.TrainerUpdateRequest;
import org.example.dto.response.MemberCreateResponse;
import org.example.dto.response.TrainerResponse;
import org.example.dto.response.TrainerTrainingsResponse;
import org.example.exception.NotFoundException;
import org.example.repository.TrainerRepository;
import org.example.repository.TrainingRepository;
import org.example.specification.TrainingSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TrainerService {
    private final TrainerRepository trainerRepository;
    private final UserService userService;
    private final TrainingTypeService trainingTypeService;
    private final TrainingRepository trainingRepository;

    @Transactional
    public MemberCreateResponse createTrainer(TrainerCreateRequest createRequest) {
        User user = userService.create(createRequest.firstName(), createRequest.lastName(), createRequest.password());
        TrainingType type = trainingTypeService.get(createRequest.trainingTypeId());
        Trainer trainer = Trainer.builder()
            .user(user)
            .trainingType(type)
            .build();

        log.info("Adding " );

        trainerRepository.save(trainer);
        return Mapper.mapMemberResponse(trainer.getUser().getUsername(), trainer.getUser().getPassword());
    }

    @Transactional
    public TrainerResponse updateTrainer(Long id, TrainerUpdateRequest updateRequest) {
        Trainer current = getById(id);
        User updated = userService.updateUser(current.getUser(), updateRequest.firstName(), updateRequest.lastName(), updateRequest.isActive());
        TrainingType type = trainingTypeService.get(updateRequest.trainingTypeId());
        current.setTrainingType(type);
        current.setUser(updated);
        trainerRepository.save(current);
        log.info("Updating");

        return Mapper.mapTrainerToTrainerResponse(current);
    }

    public void updateIsActive(Long id, boolean isActive) {
        Trainer trainer = getById(id);
        userService.updateIsActive(trainer.getUser(), isActive);
    }
    public TrainerResponse get(String username) {
        log.info("Retrieving");

        Trainer trainer = getByUsername(username);
        return Mapper.mapTrainerToTrainerResponse(trainer);
    }

    public List<TrainerTrainingsResponse> getTrainings(String trainerUsername, String traineeUsername, LocalDate from, LocalDate to) {
        Specification<Training> spec = Specification.where(TrainingSpecification.hasTrainerUsername(trainerUsername))
            .and(TrainingSpecification.hasTrainerUsername(trainerUsername))
            .and(TrainingSpecification.hasFromAfter(from))
            .and(TrainingSpecification.hasToBefore(to));
        List<Training> trainings = trainingRepository.findAll(spec);

        return trainings.stream().map(Mapper::mapTrainingToTrainerTrainingResponse).toList();

    }

    public Trainer getByUsername(String username) {
        return trainerRepository.findByUser_Username(username).orElseThrow(() -> new NotFoundException("Trainer with given " + username + " username doesn't exist"));
    }

    public void addTrainee(Trainer trainer, Training training) {
        trainer.getTrainees().add(training.getTrainee());
    }

    private Trainer getById(Long id) {
        return trainerRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("Trainer with given " + id + " doesn't exist"));
    }

}
