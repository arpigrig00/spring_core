package org.example.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.dto.request.TrainingCreateRequest;
import org.example.enums.ActionType;
import org.example.exception.NotFoundException;
import org.example.repository.TrainingRepository;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TrainingService {
    private final TrainingRepository trainingRepository;
    private final TrainerService trainerService;
    private final TraineeService traineeService;
    private final TrainingWorkloadService trainingWorkloadService;

    @Transactional
    public void createTraining(TrainingCreateRequest createRequest){
        Trainer trainer = getTrainer(createRequest.trainerUsername()) ;
        Trainee trainee = getTrainee(createRequest.traineeUsername());

        Training training = Training.builder()
                .trainee(trainee)
                .trainer(trainer)
                .trainingName(createRequest.trainingName())
                .trainingDate(createRequest.date())
                .trainingDuration(createRequest.duration())
                .build();

        trainingRepository.save(training);
        traineeService.addTraining(trainee, training);
        trainerService.addTrainee(trainer,training);

        trainingWorkloadService.sendWorkloadRequest(training, ActionType.ADD);

        log.info("Adding ");
    }

    public void removeTraining(Long id) {
        Optional<Training> training = trainingRepository.findById(id);
        if(training.isEmpty()) {
            throw new NotFoundException("Training with given " + id + " id doesn't exist");
        }
        trainingRepository.delete(training.get());
    }

    private Trainee getTrainee(String username) {
        return traineeService.getByUsername(username);
    }

    private Trainer getTrainer(String username) {
        return trainerService.getByUsername(username);
    }
}
