package org.example.service;

import java.io.IOException;
import java.util.Arrays;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@RequiredArgsConstructor
public class JwtAuthFilter extends OncePerRequestFilter {
    private static final String[] WHITE_LIST_URL =
        {"/api-docs", "/api/swagger-ui/**", "/v3/api-docs/**", "/swagger-ui/**", "/api/auth/**","/api/trainings/request/micro"};
    private final JwtService jwtService;
    private final UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(@NonNull  HttpServletRequest request,@NonNull HttpServletResponse response, @NonNull FilterChain filterChain) throws ServletException, IOException {
      try {
          final String authHeader =  request.getHeader("Authorization");
          final String jwtToken;
          String username;

          if (excludeUrls.matches(request)) {
              filterChain.doFilter(request, response);
              return; // Skip JWT processing for these paths
          }

          if (authHeader == null || !authHeader.startsWith("Bearer")) {
              response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
              response.getWriter().write("Authorization header missing or invalid");
              return;
          }
          jwtToken = authHeader.substring(7);
          username = jwtService.getUsername(jwtToken);
          if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
              UserDetails userDetails = userDetailsService.loadUserByUsername(username);
              if (jwtService.isTokenValid(jwtToken, userDetails)) {
                  UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                  authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                  SecurityContextHolder.getContext().setAuthentication(authToken);
              }

          }
          filterChain.doFilter(request, response);

      } catch (ExpiredJwtException e) {
          response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
          response.getWriter().write("Token has expired");
      } catch (JwtException e) {
          response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
          response.getWriter().write("Invalid token");
      }
    }

    private final RequestMatcher excludeUrls = new OrRequestMatcher(
        new AntPathRequestMatcher("/trainee", "POST"),
        new AntPathRequestMatcher("/trainer", "POST"),
        new AntPathRequestMatcher("/trainings/request/micro", "GET"),
        new AntPathRequestMatcher("/auth/login", "POST"),
        new OrRequestMatcher(
            Arrays.stream(WHITE_LIST_URL)
                .map(AntPathRequestMatcher::new)
                .toArray(RequestMatcher[]::new)
        )
    );
}
