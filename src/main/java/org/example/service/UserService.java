package org.example.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.request.PasswordChangeRequest;
import org.example.exception.NotFoundException;
import org.example.exception.PasswordNotMatchException;
import org.example.utils.PasswordGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.example.domain.User;
import org.example.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService{
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public User create(String firstName, String lastName, String password) {
        User user = User.builder()
            .firstName(firstName)
            .lastName(lastName)
            .username(createUsername(firstName, lastName))
            .password(passwordEncoder.encode(password))
            .isActive(true)
            .build();

        userRepository.save(user);

        log.info("Adding");

        return user;
    }

    public User updateUser(User user, String firstName,  String lastName, boolean isActive){
        user.setIsActive(isActive);
        user.setFirstName(firstName);
        user.setLastName(lastName);

        log.info("Updating");

        return userRepository.save(user);
    }

    public void updatePassword(PasswordChangeRequest passwordChangeRequest) {
        User user = findByUsername(passwordChangeRequest.username());
        boolean matches = passwordEncoder.matches(user.getPassword(), passwordEncoder.encode(passwordChangeRequest.oldPassword()));
        if (matches) {
            user.setPassword(passwordEncoder.encode(passwordChangeRequest.newPassword()));

        } else {
            throw new PasswordNotMatchException();
        }
        log.info("Updating");

        userRepository.save(user);
    }

    public void updateIsActive(User user, boolean isActive) {
        user.setIsActive(isActive);
        log.info("Updating");

        userRepository.save(user);
    }
    private String createUsername(String firstName, String lastName) {
        String username = firstName + "." + lastName;
        int existUsernameCount = getCountByUsername(username);
        if (existUsernameCount > 0) {
            username = username + (existUsernameCount + 1);
        }
        return username;
    }

    private int getCountByUsername(String username) {
        return userRepository.findCountByUsername(username);
    }
    private User getById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new NotFoundException("User with given " + id + " doesn't exist"));
    }


    private User findByUsername(String username) {
        log.info("Retrieving");
        return userRepository.findByUsername(username).orElseThrow(() -> new NotFoundException("User with given " + username + " doesn't exist"));
    }
}
