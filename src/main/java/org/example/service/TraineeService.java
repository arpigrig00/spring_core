package org.example.service;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.request.TrainerWorkloadRequest;
import org.example.enums.ActionType;
import org.example.exception.NotFoundException;
import org.example.specification.TrainingSpecification;
import org.springframework.data.jpa.domain.Specification;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.User;
import org.example.dto.request.TraineeCreateRequest;
import org.example.dto.request.TraineeUpdateRequest;
import org.example.dto.response.MemberCreateResponse;
import org.example.dto.response.TraineeResponse;
import org.example.dto.response.TraineeTrainerResponse;
import org.example.dto.response.TraineeTrainingsResponse;
import org.example.repository.TraineeRepository;
import org.example.repository.TrainerRepository;
import org.example.repository.TrainingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class TraineeService {
    private final TraineeRepository traineeRepository;

    private final UserService userService;

    private final TrainingRepository trainingRepository;

    private final TrainerRepository trainerRepository;

    private final TrainingWorkloadService trainingWorkloadService;
    @Transactional
    public MemberCreateResponse createTrainee(TraineeCreateRequest createDto) {
        User user = userService.create(createDto.firstName(), createDto.lastName(), createDto.password());

        Trainee trainee = Trainee.builder()
            .user(user)
            .address(createDto.address())
            .dob(createDto.dob())
            .build();
        log.info("Adding");
        traineeRepository.save(trainee);

        return Mapper.mapMemberResponse(trainee.getUser().getUsername(), trainee.getUser().getPassword());
    }

    public TraineeResponse updateTrainee(Long id, TraineeUpdateRequest updateDto) {
        Trainee current = getTrainee(id);
        User user = userService.updateUser(current.getUser(), updateDto.firstName(), updateDto.lastName(),
                                           updateDto.isActive());
        current.setDob(updateDto.dob());
        current.setAddress(updateDto.address());
        current.setUser(user);
        traineeRepository.save(current);
        log.info("Updating");
        return Mapper.mapTraineeToTraineeResponse(current);
    }

    public List<TraineeTrainerResponse> getUnrelatedTrainers(Long id) {
        Trainee trainee = getTrainee(id);

        List<Long> relatedTrainerIds = trainee.getTrainers().stream().map(Trainer::getId).toList();
        List<Trainer> trainers = trainerRepository.findAllByIdNotIn(relatedTrainerIds);
        return trainers.stream().map(Mapper::mapTrainerToTraineeTrainerResponse).toList();
    }

    public void updateIsActive(Long id, boolean isActive) {
        Trainee trainee = getTrainee(id);
        userService.updateIsActive(trainee.getUser(), isActive);
    }

    @Transactional
    public void delete(Long id) {
        List<TrainerWorkloadRequest> workload = new ArrayList<>();
        Trainee trainee = getTrainee(id);
        log.info("Removing");
        Set<Training> trainings = trainee.getTrainings();
        trainings.forEach((t) -> {
            TrainerWorkloadRequest request = addTrainingToWorkload(t);
            workload.add(request);
        });

        traineeRepository.delete(trainee);
        //trainingWorkloadService.sendBulkWorkloadRequest(workload);
    }

    public TraineeResponse get(Long id) {
        log.info("Retrieving ");
        Trainee trainee = getTrainee(id);
        return Mapper.mapTraineeToTraineeResponse(trainee);
    }

    public List<TraineeTrainingsResponse> getTrainings(String traineeUsername, String trainerUsername, LocalDate from,
                                                       LocalDate to) {
        Specification<Training> spec = Specification.where(TrainingSpecification.hasTraineeUsername(traineeUsername))
            .and(TrainingSpecification.hasTrainerUsername(trainerUsername))
            .and(TrainingSpecification.hasFromAfter(from))
            .and(TrainingSpecification.hasToBefore(to));

        List<Training> trainings = trainingRepository.findAll(spec);
        return trainings.stream().map(Mapper::mapTrainingToTraineeTrainingResponse).toList();
    }

    public void addTraining(Trainee trainee, Training training) {
        Set<Training> trainings = trainee.getTrainings();
        Set<Trainer> trainers = trainee.getTrainers();
        trainings.add(training);
        trainers.add(training.getTrainer());
        traineeRepository.save(trainee);
    }

    @Transactional
    public List<TraineeTrainerResponse> updateTrainings(Long id, List<String> usernames) {
        Trainee trainee = getTrainee(id);
        List<Trainer> trainers = trainerRepository.findAllByUser_UsernameIsIn(usernames);

        removeTraineeFromOldTrainers(trainee);
        trainee.getTrainers().clear();
        addTraineeToNewTrainers(trainee, trainers);
        traineeRepository.save(trainee);

        return trainers.stream().map(Mapper::mapTrainerToTraineeTrainerResponse).toList();
    }

    private void addTraineeToNewTrainers(Trainee trainee, List<Trainer> trainers) {
        Set<Trainer> newTrainers = new HashSet<>(trainers);

        trainee.setTrainers(newTrainers);

        trainers.forEach(t -> {
            Set<Trainee> tr = t.getTrainees();
            tr.add(trainee);
            trainerRepository.save(t);
        });
    }

    private void removeTraineeFromOldTrainers(Trainee trainee) {
        Set<Trainer> oldTrainers = trainee.getTrainers();

        oldTrainers.forEach(t -> {
            Set<Trainee> tr = t.getTrainees();
            tr.remove(trainee);
            trainerRepository.save(t);
        });
    }

    public Trainee getByUsername(String username) {
        return traineeRepository.findByUser_Username(username)
            .orElseThrow(() -> new NotFoundException("Trainee with given " + username + " username doesn't exist"));
    }

    private Trainee getTrainee(Long id) {
        log.info("Retrieving ");
        return traineeRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("Trainee with given " + id + " doesn't exist"));
    }

    private TrainerWorkloadRequest addTrainingToWorkload(Training training) {
        return Mapper.mapTrainingToTrainerWorkloadRequest(training, ActionType.DELETE);
    }
}
