package org.example.domain;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PreRemove;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Trainee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @NotNull
    private User user;
    private LocalDate dob;
    private String address;

    @ManyToMany(mappedBy = "trainees", fetch = FetchType.EAGER)
    private Set<Trainer> trainers = new HashSet<>();

    public Set<Trainer> getTrainers() {
        if (trainers == null) {
            trainers = new HashSet<>();
        }
        return trainers;
    }

    @OneToMany(mappedBy = "trainee", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Training> trainings = new HashSet<>();

    public Set<Training> getTrainings() {
        if (trainings == null) {
            trainings = new HashSet<>();
        }
        return trainings;
    }
    @PreRemove
    private void removeTraineesFromTrainers() {
        for (Trainer trainer : trainers) {
            trainer.getTrainees().remove(this);
        }
    }
}
