package org.example.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import org.example.validator.PasswordValidator;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {PasswordValidator.class})
public @interface Password {

        String message() default "Password should meet the following criteria: at least 12 characters long, contain at least one number, one special character , one uppercase letter and one lowercase letter.";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
}
