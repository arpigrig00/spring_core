Feature: Create a Trainee

  Scenario: Successfully create a trainee
    Given a user exists with firstName "John", lastName "Doe", and password "pass123"
    When I create a trainee with address "123 Main St" and date of birth "1990-01-01"
    Then the trainee should be saved and I should receive a response with username "John.Doe"
