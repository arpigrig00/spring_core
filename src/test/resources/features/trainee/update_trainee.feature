Feature: Update Trainee

  Scenario: Successfully update an existing trainee
    Given a trainee exists with id 1 and the following details:
      | firstName | lastName | dob       | address        | isActive |
      | John      | Doe      | 1990-01-01| 123 Main St    | true     |
    When I update the trainee with id 1 to the following details:
      | firstName | lastName | dob       | address        | isActive |
      | Jane      | Smith    | 1992-05-05| 456 Maple Ave  | false    |
    Then the trainee with id 1 should have the following details:
      | firstName | lastName | dob       | address        | isActive |
      | Jane      | Smith    | 1992-05-05| 456 Maple Ave  | false    |
