Feature: Delete a trainee

  Scenario: Successfully delete an existing trainee
    Given a trainee exists with id 1 and the following trainings:
      | trainingId | trainerId |
      | 200        | 100       |
      | 201        | 101       |
    When I delete the trainee with id 1
    Then the trainee with id 1 should no longer exist in the system
    And the following trainings should be processed for workload:
      | trainingId |
      | 200        |
      | 201        |
