Feature: Get unrelated trainers for a trainee

  Scenario: Successfully retrieve trainers unrelated to the trainee
    Given a trainee exists with id 1 and the following trainers:
      | trainerId | related |
      | 100       | true    |
      | 101       | false   |
      | 102       | false   |
    When I request the unrelated trainers for trainee with id 1
    Then I should get the following unrelated trainers:
      | trainerId |
      | 101       |
      | 102       |
