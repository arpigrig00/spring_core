Feature: Retrieve a trainer by username

  Scenario: Successfully retrieve an existing trainer by username
    Given a trainer exists with username "john_doe" and the following details:
      | firstName | lastName | password | trainingTypeId | isActive |
      | John      | Doe      | pass123  | 1              | true     |
    When I retrieve the trainer with username "john_doe"
    Then I should get the following trainer details:
      | firstName | lastName | trainingTypeId | isActive |
      | John      | Doe      | 1              | true     |
