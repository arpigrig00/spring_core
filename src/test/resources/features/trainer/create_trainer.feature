Feature: Create a new trainer

  Scenario: Successfully create a new trainer
    Given a training type with id 1 exists
    When I create a trainer with the following details:
      | firstName | lastName | password | trainingTypeId |
      | John      | Smith    | pass123  | 1              |
    Then the trainer should be successfully created with the username "JohnSmith"
    And the trainer's password should be "pass123"
