Feature: Update a trainer

  Scenario: Successfully update an existing trainer
    Given a trainer exists with id 1 and the following details:
      | firstName | lastName | password | trainingTypeId | isActive |
      | John      | Doe      | pass123  | 1              | true     |
    When I update the trainer with id 1 to the following details:
      | firstName | lastName | trainingTypeId | isActive |
      | Jane      | Smith    | 2              | false    |
    Then the trainer with id 1 should have the following details:
      | firstName | lastName | trainingTypeId | isActive |
      | Jane      | Smith    | 2              | false    |
