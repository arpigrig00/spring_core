Feature: Create a new training

  Scenario: Successfully create a new training
    Given a trainer exists with username "john_doe" and the following details:
      | firstName | lastName | password | trainingTypeId | isActive |
      | John      | Doe      | pass123  | 1              | true     |
    And a trainee exists with username "jane_doe" and the following details:
      | firstName | lastName | password | address    | dob        |
      | Jane      | Doe      | pass123  | 123 Street | 1990-01-01 |
    When I create a training with the following details:
      | trainerUsername | traineeUsername | trainingName   | date       | duration |
      | john_doe        | jane_doe        | Yoga Session   | 2023-09-10 | 60       |
    Then the training should be successfully created for trainee "jane_doe" with trainer "john_doe"
