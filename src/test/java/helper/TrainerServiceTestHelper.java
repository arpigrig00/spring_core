package helper;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.TrainingType;
import org.example.dto.request.TrainerCreateRequest;
import org.example.dto.request.TrainerUpdateRequest;
import org.example.dto.request.TrainingListQueryRequest;

public interface TrainerServiceTestHelper extends UserServiceTestHelper {
    Long ID = 1L;
    String TRAINING_TYPE = "YOGA";

    default Trainer newTrainer(Long id) {
        Set<Trainee> trainees = new HashSet<>();
        trainees.add(newTrainee(2L));
        return Trainer.builder()
            .id(id)
            .user(newUser(ID))
            .trainingType(TrainingType.builder().trainingTypeName(TRAINING_TYPE).build())
            .trainees(trainees)
            .build();
    }

    default Trainer updatedTrainer(Long id) {
        return Trainer.builder()
            .id(id)
            .user(updatedUser(ID))
            .trainingType(TrainingType.builder().trainingTypeName(TRAINING_TYPE).build())
            .build();
    }

    default Training newTraining(Long id) {
        return Training.builder()
            .id(id)
            .trainingDate(LocalDate.parse("2024-05-05"))
            .trainingName("YOGA")
            .trainingDuration(4L)
            .trainee(newTrainee(1L))
            .trainer(newTrainer(ID))
            .build();
    }

    default Trainee newTrainee(Long id) {
        return Trainee.builder()
            .id(id)
            .user(newTraineeUser(2L))
            .dob(LocalDate.parse("1990-05-06"))
            .address("ADDRESS")
            .build();
    }

    default TrainerCreateRequest createDto() {
        return new TrainerCreateRequest(FIRSTNAME, LASTNAME, ID, PASSWORD);
    }

    default TrainerUpdateRequest updateDto() {
        return new TrainerUpdateRequest(false, 1L, UPDATED_FIRSTNAME, UPDATED_LASTNAME);
    }

    default TrainingListQueryRequest listQueryDto() {
        return new TrainingListQueryRequest(TRAINER_USERNAME, TRAINEE_USERNAME, LocalDate.parse("2024-01-01"),
                                            LocalDate.parse("2026-01-01"));
    }
}
