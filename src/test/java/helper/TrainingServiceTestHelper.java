package helper;

import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.TrainingType;
import org.example.dto.request.TrainingCreateRequest;

import java.time.LocalDate;

public interface TrainingServiceTestHelper extends UserServiceTestHelper {
    Long ID  = 1L;
    String TRAINING_NAME = "Daily training";
    String TRAINER_NAME = "Trainer";
    String TRAINEE_NAME = "Trainee";
    String TRAINING_TYPE = "BOX";
    Long TRAINING_DURATION = 3L;

    LocalDate TRAINING_DATE = LocalDate.parse("2024-07-06");
    LocalDate DOB = LocalDate.parse("2000-03-03");
    String ADDRESS = "Address 1";

    default Trainer newTrainer(Long id) {
                return Trainer.builder()
                        .id(id)
                        .user(newUser(ID))
                        .trainingType(TrainingType.builder().trainingTypeName(TRAINING_TYPE).build())
                        .build();
            }

    default Trainee newTrainee(Long id) {
        return Trainee.builder()
            .id(id)
            .user(updatedUser(2L))
            .dob(DOB)
            .address(ADDRESS)
            .build();
    }


    default Training newTraining(Long id) {
        return Training.builder()
                .id(id)
                .trainingDate(TRAINING_DATE)
                .trainingName(TRAINING_NAME)
                .trainingDuration(TRAINING_DURATION)
                .trainee(newTrainee(ID))
                .trainer(newTrainer(ID))
                .build();
    }

    default TrainingCreateRequest createDto() {
        return new TrainingCreateRequest(TRAINER_NAME, TRAINEE_NAME, TRAINING_NAME,  TRAINING_DATE, TRAINING_DURATION);

    }
}
