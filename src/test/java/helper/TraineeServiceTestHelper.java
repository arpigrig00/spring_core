package helper;

import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.TrainingType;
import org.example.dto.request.TraineeCreateRequest;
import org.example.dto.request.TraineeUpdateRequest;
import org.example.dto.request.TrainingListQueryRequest;

import java.time.LocalDate;

public interface TraineeServiceTestHelper extends UserServiceTestHelper{
    Long ID = 1L;
     String ADDRESS = "Address 1";
     String UPDATE_ADDRESS = "456 Elm St";

     LocalDate DOB = LocalDate.now();
     LocalDate UPDATE_DOB = LocalDate.parse("2000-05-03");
    default Trainee newTrainee(Long id) {
        return Trainee.builder()
                .id(id)
                .user(newUser(1L))
                .dob(DOB)
                .address(ADDRESS)
                .build();
    }
    default Training newTraining(Long id) {
        return Training.builder()
            .id(id)
            .trainingDate(LocalDate.parse("2024-05-05"))
            .trainingName("YOGA")
            .trainingDuration(4L)
            .trainee(newTrainee(1L))
            .trainer(newTrainer(ID))
            .build();
    }

    default Trainee updatedTrainee(Long id) {
        return Trainee.builder()
            .id(id)
            .user(updatedUser(ID))
            .dob(UPDATE_DOB)
            .address(UPDATE_ADDRESS)
            .build();
    }

    default Trainer newTrainer(Long id) {
        return Trainer.builder()
            .id(id)
            .user(newUser(ID))
            .trainingType(TrainingType.builder().trainingTypeName("BOX").build())
            .build();
    }

    default Trainer unrelatedTrainer(Long id) {
        return Trainer.builder()
            .id(id)
            .user(newTrainerUser(2L))
            .trainingType(TrainingType.builder().trainingTypeName("BOX").build())
            .build();
    }

    default TraineeCreateRequest createDto() {
        return new TraineeCreateRequest(FIRSTNAME, LASTNAME, DOB, ADDRESS, PASSWORD);
    }

    default TraineeCreateRequest createTraineeRequest() {
        return new TraineeCreateRequest(FIRSTNAME, LASTNAME, null, ADDRESS, PASSWORD);
    }

    default TraineeUpdateRequest updateDto() {
        return  new TraineeUpdateRequest(UPDATED_FIRSTNAME, UPDATED_LASTNAME, Boolean.FALSE, UPDATE_DOB, UPDATE_ADDRESS);
    }

    default TrainingListQueryRequest listQueryDto() {
        return new TrainingListQueryRequest(TRAINER_USERNAME, TRAINEE_USERNAME, LocalDate.parse("2024-01-01"), LocalDate.parse("2026-01-01"));
    }
}
