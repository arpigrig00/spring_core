package helper;

import org.example.domain.User;
import org.example.dto.request.PasswordChangeRequest;
import org.example.dto.response.MemberCreateResponse;

public interface UserServiceTestHelper {
    long ID = 1L;

    String FIRSTNAME = "name";
    String UPDATED_FIRSTNAME = "firstname";
    String LASTNAME = "lastname";
    String UPDATED_LASTNAME = "lastname1";
    String USERNAME = "name.lastname";
    String UPDATED_USERNAME = "firstname.lastname1";
    String PASSWORD = "abcd123456";
    String UPDATED_PASSWORD = "efgh123456";

    String TRAINER_FIRSTNAME = "name";
    String TRAINER_LASTNAME = "lastname";
    String TRAINER_USERNAME = "name.lastname";
    String TRAINER_PASSWORD = "ased123456";
    String TRAINEE_FIRSTNAME = "user";
    String TRAINEE_LASTNAME = "john";
    String TRAINEE_USERNAME = "user.john";
    String TRAINEE_PASSWORD = "asefdt3456";

    default User newUser(Long id) {
        return User.builder()
            .id(id)
            .firstName(FIRSTNAME)
            .lastName(LASTNAME)
            .username(USERNAME)
            .password(PASSWORD)
            .isActive(true)
            .build();
    }

    default User newTraineeUser(Long id) {
        return User.builder()
            .id(id)
            .firstName(TRAINEE_FIRSTNAME)
            .lastName(TRAINEE_LASTNAME)
            .username(TRAINEE_USERNAME)
            .password(TRAINEE_PASSWORD)
            .isActive(true)
            .build();
    }

    default User newTrainerUser(Long id) {
        return User.builder()
            .id(id)
            .firstName(TRAINER_FIRSTNAME)
            .lastName(TRAINER_LASTNAME)
            .username(TRAINER_USERNAME)
            .password(TRAINER_PASSWORD)
            .isActive(true)
            .build();
    }

    default User updatedUser(Long id) {
        return User.builder()
            .id(id)
            .firstName(UPDATED_FIRSTNAME)
            .lastName(UPDATED_LASTNAME)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .isActive(false)
            .build();
    }


    default MemberCreateResponse memberCreateResponse() {
        return MemberCreateResponse
            .builder()
            .username(USERNAME)
            .password(PASSWORD)
            .build();
    }

    default PasswordChangeRequest changePassword() {
        return PasswordChangeRequest
            .builder()
            .username(USERNAME)
            .oldPassword(PASSWORD)
            .newPassword(UPDATED_PASSWORD)
            .build();
    }
    default PasswordChangeRequest changeInvalidPassword() {
        return PasswordChangeRequest
            .builder()
            .username(USERNAME)
            .oldPassword("other password")
            .newPassword(UPDATED_PASSWORD)
            .build();
    }
}
