package controller;

import static org.mockito.Mockito.mock;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.example.controller.TrainingTypesController;
import org.example.dto.response.TrainingTypeResponse;
import org.junit.jupiter.api.Test;
import org.example.service.TrainingTypeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class TrainingTypeControllerTest {
    private static MockMvc mockMvc;
    @InjectMocks
    private TrainingTypesController trainingTypesController;

    private final TrainingTypeService trainingTypeService = mock(TrainingTypeService.class);

    private ObjectMapper mapper;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(trainingTypesController).build();

        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.findAndRegisterModules();
    }

    @Test
    public void testGetAllTrainingTypes() throws Exception {
        TrainingTypeResponse response = TrainingTypeResponse.builder().id(1L).trainingType("Yoga").build();

        when(trainingTypeService.getAll()).thenReturn(Collections.singletonList(response));

        mockMvc.perform(get("/training-types"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(response.id()))
            .andExpect(jsonPath("$[0].trainingType").value(response.trainingType()));

        verify(trainingTypeService).getAll();
    }
}
