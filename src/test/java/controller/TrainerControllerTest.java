package controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import helper.TrainerServiceTestHelper;
import org.example.controller.TrainerController;
import org.example.domain.Trainer;
import org.example.dto.request.TrainerCreateRequest;
import org.example.dto.request.TrainerUpdateRequest;
import org.example.dto.response.MemberCreateResponse;
import org.example.dto.response.TrainerResponse;
import org.example.service.Mapper;
import org.example.service.TrainerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
public class TrainerControllerTest implements TrainerServiceTestHelper {
    private static MockMvc mockMvc;
    @InjectMocks
    private TrainerController trainerController;

    private final TrainerService trainerService = mock(TrainerService.class);

    private ObjectMapper mapper;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(trainerController).build();

        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.findAndRegisterModules();
    }

    @Test
    public void testGetTrainer() throws Exception {
        Trainer trainer = newTrainer(1L);
        TrainerResponse response = Mapper.mapTrainerToTrainerResponse(trainer);
        when(trainerService.get(any(String.class))).thenReturn(response);

        mockMvc.perform(get("/trainer/{id}",trainer.getUser().getId() ))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").isNotEmpty())
            .andExpect(jsonPath("$.username").value(trainer.getUser().getUsername()))
            .andExpect(jsonPath("$.firstName").value(trainer.getUser().getFirstName()))
            .andExpect(jsonPath("$.lastName").value(trainer.getUser().getLastName()));
    }

    @Test
    public void testCreateTrainee() throws Exception {
        MemberCreateResponse response = memberCreateResponse();
        TrainerCreateRequest request = createDto();

        when(trainerService.createTrainer(any(TrainerCreateRequest.class))).thenReturn(response);

        mockMvc.perform(post("/trainer")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(mapper.writeValueAsString(request)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.username").value(response.username()))
            .andExpect(jsonPath("$.password").value(response.password()));
    }

    @Test
    public void testUpdateTrainee() throws Exception {
        Trainer trainer = newTrainer(1L);
        TrainerUpdateRequest request = updateDto();
        TrainerResponse response = Mapper.mapTrainerToTrainerResponse(trainer);

        when(trainerService.updateTrainer(eq(1L), any(TrainerUpdateRequest.class))).thenReturn(response);
        mockMvc.perform(put("/trainer/1")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(mapper.writeValueAsString(request)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.username").value(response.username()))
            .andExpect(jsonPath("$.firstName").value(response.firstName()))
            .andExpect(jsonPath("$.lastName").value(response.lastName()));
    }

    @Test
    public void testChangeStatus() throws Exception {
        mockMvc.perform(patch("/trainer/" + ID + "/activate/true"))
            .andExpect(status().isOk());
    }
}
