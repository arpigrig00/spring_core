package controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import helper.UserServiceTestHelper;
import org.example.controller.UserController;
import org.example.service.AuthService;
import org.example.dto.request.PasswordChangeRequest;
import org.example.dto.request.UserCheckRequest;
import org.example.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.junit.jupiter.api.Test;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest implements UserServiceTestHelper {
    private static MockMvc mockMvc;
    @InjectMocks
    private UserController userController;

    private final AuthService authService = mock(AuthService.class);
    private final UserService userService = mock(UserService.class);

    private ObjectMapper mapper;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();

        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.findAndRegisterModules();
    }

    @Test
    public void testChangePassword() throws Exception {
        PasswordChangeRequest request = changePassword();

        mockMvc.perform(put("/users/change-password")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(mapper.writeValueAsString(request)))
            .andExpect(status().isNoContent());

        verify(userService).updatePassword(any(PasswordChangeRequest.class));

    }

    @Test
    public void testCheckUser() throws Exception {
        UserCheckRequest request = UserCheckRequest
            .builder()
            .username("testUser")
            .password("testPass")
            .build();

        mockMvc.perform(post("/users/check")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(mapper.writeValueAsString(request)))
            .andExpect(status().isOk());

        verify(authService).checkAuthUser(any(UserCheckRequest.class));
    }
}
