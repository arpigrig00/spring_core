package controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import helper.TraineeServiceTestHelper;
import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.dto.request.TraineeCreateRequest;
import org.example.dto.request.TraineeUpdateRequest;
import org.example.dto.response.TraineeResponse;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.example.dto.response.TraineeTrainerResponse;
import org.example.service.Mapper;
import org.example.service.TraineeService;
import org.example.controller.TraineeController;
import org.example.dto.response.MemberCreateResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.mockito.InjectMocks;

import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
public class TraineeControllerTest implements TraineeServiceTestHelper {

    private static MockMvc mockMvc;
    @InjectMocks
    private TraineeController traineeController;

    private final TraineeService traineeService = mock(TraineeService.class);


    private ObjectMapper mapper;
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(traineeController).build();

        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.findAndRegisterModules();
    }

    @Test
    public void testGetTrainee() throws Exception {
        Trainee trainee = newTrainee(1L);
        TraineeResponse response = Mapper.mapTraineeToTraineeResponse(trainee);
        when(traineeService.get(any(Long.class))).thenReturn(response);

        mockMvc.perform(get("/trainee/{id}",trainee.getId()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").isNotEmpty())
            .andExpect(jsonPath("$.username").value(trainee.getUser().getUsername()))
            .andExpect(jsonPath("$.firstName").value(trainee.getUser().getFirstName()))
            .andExpect(jsonPath("$.lastName").value(trainee.getUser().getLastName()));
    }

    @Test
    public void testCreateTrainee() throws Exception {
        MemberCreateResponse response = memberCreateResponse();
        TraineeCreateRequest request = createTraineeRequest();
        ;
        when(traineeService.createTrainee(any(TraineeCreateRequest.class))).thenReturn(response);

        mockMvc.perform(post("/trainee")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(mapper.writeValueAsString(request)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.username").value(response.username()))
            .andExpect(jsonPath("$.password").value(response.password()));
    }

    @Test
    public void testUpdateTrainee() throws Exception {
        Trainee trainee = newTrainee(1L);
        TraineeUpdateRequest request = updateDto();
        TraineeResponse response = Mapper.mapTraineeToTraineeResponse(trainee);
        when(traineeService.updateTrainee(eq(1L), any(TraineeUpdateRequest.class))).thenReturn(response);

        mockMvc.perform(put("/trainee/1")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(mapper.writeValueAsString(request)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.username").value(response.username()))
            .andExpect(jsonPath("$.address").value(response.address()))
            .andExpect(jsonPath("$.firstName").value(response.firstName()))
            .andExpect(jsonPath("$.lastName").value(response.lastName()));
    }

    @Test
    public void testGetUnrelatedTrainers() throws Exception {
        Trainer trainer = unrelatedTrainer(1L);
        List<TraineeTrainerResponse> response = List.of(Mapper.mapTrainerToTraineeTrainerResponse(trainer));
        when(traineeService.getUnrelatedTrainers(any(Long.class))).thenReturn(response);

        mockMvc.perform(get("/trainee/" + ID +"/unrelated-trainers"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.length()").value(response.size()));
    }

    @Test
    public void testDeleteTrainee() throws Exception {
        mockMvc.perform(delete("/trainee/" + ID))
            .andExpect(status().isNoContent());
    }

    @Test
    public void testChangeStatus() throws Exception {
        mockMvc.perform(patch("/trainee/" + ID + "/activate/true"))
            .andExpect(status().isOk());
    }

    @Test
    public void testChangeTrainers() throws Exception {
        List<TraineeTrainerResponse> response = List.of(Mapper.mapTrainerToTraineeTrainerResponse(newTrainer(1L)));
        when(traineeService.updateTrainings(eq(ID), anyList())).thenReturn(response);

        mockMvc.perform(put("/trainee/${id}/change-trainers", ID)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\"trainersUsername\":[\"somename\"]}"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.length()").value(1));
    }
}
