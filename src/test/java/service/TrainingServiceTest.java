package service;

import helper.TrainingServiceTestHelper;

import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.TrainingType;
import org.example.dto.request.TrainingCreateRequest;
import org.example.repository.TrainingRepository;
import org.example.service.TraineeService;
import org.example.service.TrainerService;
import org.example.service.TrainingService;
import org.example.service.TrainingTypeService;
import org.example.service.TrainingWorkloadService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TrainingServiceTest implements TrainingServiceTestHelper {
    private static TrainingRepository trainingRepository;
    private static TraineeService traineeService;

    private static TrainerService trainerService;

    private static TrainingService trainingService;
    private static TrainingTypeService trainingTypeService;

    @BeforeEach
    public void setUp() {
        trainingRepository = mock(TrainingRepository.class);
        traineeService = mock(TraineeService.class);
        trainerService = mock(TrainerService.class);
        trainingTypeService = mock(TrainingTypeService.class);
        TrainingWorkloadService trainingWorkloadService = mock(TrainingWorkloadService.class);
        trainingService = new TrainingService(trainingRepository, trainerService, traineeService,
                                              trainingWorkloadService);
    }

    @Test
    void testCreateTraining() {
        TrainingCreateRequest createDto = createDto();
        Trainer trainer = newTrainer(ID);
        Trainee trainee = newTrainee(ID);
        TrainingType trainingType = TrainingType.builder().trainingTypeName(TRAINING_TYPE).build();
        Training training = newTraining(ID);
        when(trainerService.getByUsername(any(String.class))).thenReturn(trainer);
        when(trainingTypeService.create(any(String.class))).thenReturn(trainingType);
        when(traineeService.getByUsername(any(String.class))).thenReturn(trainee);
        when(trainingRepository.save(any(Training.class))).thenReturn(training);

        trainingService.createTraining(createDto);

        verify(trainingRepository, times(1)).save(any(Training.class));
    }

    @Test
    void testCreateThrowNotFoundTrainer() {
        TrainingCreateRequest createDto = createDto();

        when(trainerService.getByUsername(any(String.class))).thenThrow(RuntimeException.class);
        assertThrows(RuntimeException.class, () -> trainingService.createTraining(createDto));
    }

    @Test
    void testCreateThrowNotFoundTrainee() {
        TrainingCreateRequest createDto = createDto();
        Trainer trainer = newTrainer(1L);
        when(trainerService.getByUsername(any(String.class))).thenReturn(trainer);
        when(traineeService.getByUsername(any(String.class))).thenThrow(RuntimeException.class);
        assertThrows(RuntimeException.class, () -> trainingService.createTraining(createDto));
    }
}
