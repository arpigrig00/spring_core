package service;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import org.example.domain.TrainingType;
import org.example.exception.NotFoundException;
import org.example.repository.TrainingTypeRepository;
import org.example.service.TrainingTypeService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.mockito.Mockito.*;

import java.util.Optional;

public class TrainingTypeServiceTest {
    private static TrainingTypeService trainingTypeService;
    private static TrainingTypeRepository trainingTypeRepository;
    @BeforeAll
    public static void setUp() {
        trainingTypeRepository = mock(TrainingTypeRepository.class);
        trainingTypeService = new TrainingTypeService(trainingTypeRepository);
    }
    @Test
    void testGet_TrainingTypeExists() {
        Long id = 1L;
        TrainingType trainingType = TrainingType.builder().trainingTypeName("BOX").build();

        when(trainingTypeRepository.findById(id)).thenReturn(Optional.of(trainingType));

        TrainingType foundTrainingType = trainingTypeService.get(id);

        assertEquals(trainingType, foundTrainingType);
    }

    @Test
    void testGet_TrainingTypeDoesNotExist() {
        Long id = 1L;

        when(trainingTypeRepository.findById(id)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(NotFoundException.class, () -> trainingTypeService.get(id));

        assertEquals("Training type with given "  + id + " doesn't exist", exception.getMessage());
    }

    @Test
    void testCreate() {
        String type = "Yoga";
        TrainingType trainingType = TrainingType.builder().trainingTypeName(type).build();

        when(trainingTypeRepository.save(any(TrainingType.class))).thenReturn(trainingType);

        TrainingType createdTrainingType = trainingTypeService.create(type);

        ArgumentCaptor<TrainingType> trainingTypeCaptor = ArgumentCaptor.forClass(TrainingType.class);
        verify(trainingTypeRepository).save(trainingTypeCaptor.capture());
        TrainingType savedTrainingType = trainingTypeCaptor.getValue();

        assertEquals(type, savedTrainingType.getTrainingTypeName());
        assertEquals(trainingType, createdTrainingType);
    }
}
