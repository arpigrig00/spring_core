package service;

import helper.TraineeServiceTestHelper;
import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.dto.request.TraineeCreateRequest;
import org.example.dto.request.TraineeUpdateRequest;
import org.example.dto.request.TrainingListQueryRequest;
import org.example.dto.response.MemberCreateResponse;
import org.example.dto.response.TraineeResponse;
import org.example.dto.response.TraineeTrainerResponse;
import org.example.dto.response.TraineeTrainingsResponse;
import org.example.repository.TraineeRepository;
import org.example.repository.TrainerRepository;
import org.example.repository.TrainingRepository;
import org.example.service.TraineeService;
import org.example.service.TrainingWorkloadService;
import org.example.service.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TraineeServiceTest implements TraineeServiceTestHelper {
    private static TraineeRepository traineeRepository;
    private static UserService userService;
    private static TrainerRepository trainerRepository;
    private static TrainingRepository trainingRepository;
    private static TraineeService traineeService;
    private static TrainingWorkloadService trainingWorkloadService;

    @BeforeAll
    public static void setUp() {
        traineeRepository = mock(TraineeRepository.class);
        trainingRepository = mock(TrainingRepository.class);
        userService = mock(UserService.class);
        trainerRepository = mock(TrainerRepository.class);
        trainingWorkloadService = mock(TrainingWorkloadService.class);
        traineeService = new TraineeService(traineeRepository, userService, trainingRepository, trainerRepository, trainingWorkloadService);
    }

    @Test
    void testCreateTrainee() {

        TraineeCreateRequest createDto = createDto();
        when(userService.create(createDto.firstName(), createDto.lastName(), createDto.password())).thenReturn(newUser(1L));
        when(traineeRepository.save(any(Trainee.class))).thenReturn(newTrainee(1L));

        MemberCreateResponse createdTrainee = traineeService.createTrainee(createDto);

        assertNotNull(createdTrainee);
        assertEquals(createDto.firstName() + "."+createDto.lastName(), createdTrainee.username());
        verify(traineeRepository, times(1)).save(any(Trainee.class));
    }

    @Test
    void testDeleteTrainee() {

        Trainee trainee = newTrainee(ID);
        when(traineeRepository.findById(any(long.class))).thenReturn(Optional.ofNullable(trainee));

        traineeService.delete(ID);

        assert trainee != null;
        verify(traineeRepository, times(1)).delete(trainee);
    }

    @Test
    void testDeleteTraineeThrowsException() {

        when(traineeRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () -> traineeService.delete(ID));

        assertEquals("Trainee with given " + ID + " doesn't exist", exception.getMessage());
    }

    @Test
    void testUpdateTrainee() {
        TraineeUpdateRequest updateDto = updateDto();

        Trainee trainee = newTrainee(1L);
        Trainee updated = updatedTrainee(1L);
        assert trainee != null;

        when(traineeRepository.findById(any(Long.class))).thenReturn(Optional.of(trainee));
        when(traineeRepository.save(any(Trainee.class))).thenReturn(updated);
        when(userService.updateUser(trainee.getUser(),updateDto.firstName(), updateDto.lastName(), updateDto.isActive() )).thenReturn(updated.getUser());

        traineeService.updateTrainee(trainee.getId(), updateDto);

        verify(traineeRepository, times(1)).save(trainee);

    }

    @Test
    void testGetTraineeByUsername() {
        Trainee trainee = newTrainee(1L);

        when(traineeRepository.findById(trainee.getId())).thenReturn(Optional.of(trainee));

        TraineeResponse result = traineeService.get(trainee.getId());

        assertNotNull(result);
        assertEquals(trainee.getUser().getUsername(), result.username());
    }

    @Test
    void testUpdateTrainerStatus() {

        Trainee trainee = newTrainee(1L);
        when(traineeRepository.findById(any(Long.class))).thenReturn(Optional.ofNullable(trainee));

        assert trainee != null;
        traineeService.updateIsActive(trainee.getId(), Boolean.FALSE);

        verify(userService, times(1)).updateIsActive(trainee.getUser(), Boolean.FALSE);
    }
    @Test
    public void testGetUnrelatedTrainers() {
        Trainee trainee = newTrainee(1L);
        Trainer anotherTrainer = unrelatedTrainer(2L);
        when(traineeRepository.findById(any(Long.class))).thenReturn(Optional.ofNullable(trainee));
        Set<Trainer> trainers = new HashSet<>();
        trainers.add(newTrainer(1L));
        assert trainee != null;
        trainee.setTrainers(trainers);

        List<Long> ids = trainers.stream().map(Trainer::getId).toList();

        when(trainerRepository.findAllByIdNotIn(ids)).thenReturn(List.of(anotherTrainer));

        List<TraineeTrainerResponse> unrelatedTrainers = traineeService.getUnrelatedTrainers(trainee.getUser().getId());

        assertEquals(ids.size(), unrelatedTrainers.size());
    }

    @Test
    void testGetTrainings() {
        TrainingListQueryRequest listQueryDto = listQueryDto();
        LocalDate from = listQueryDto.from();
        LocalDate to = listQueryDto.to();
        List<Training> trainings = List.of(newTraining(2L));
        when(trainingRepository.findAll(any(Specification.class))).thenReturn(trainings);

        List<TraineeTrainingsResponse>
            queriedTrainings = traineeService.getTrainings(listQueryDto.traineeUsername(), listQueryDto.trainerUsername(), from, to);

        assertNotNull(queriedTrainings);
        assertEquals(trainings.size(),  queriedTrainings.size());
    }
}
