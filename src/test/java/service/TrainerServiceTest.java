package service;

import helper.TrainerServiceTestHelper;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.TrainingType;
import org.example.dto.request.TrainerCreateRequest;
import org.example.dto.request.TrainerUpdateRequest;
import org.example.dto.request.TrainingListQueryRequest;
import org.example.dto.response.MemberCreateResponse;
import org.example.dto.response.TrainerResponse;
import org.example.dto.response.TrainerTrainingsResponse;
import org.example.repository.TrainerRepository;
import org.example.repository.TrainingRepository;
import org.example.service.TrainerService;
import org.example.service.TrainingTypeService;
import org.example.service.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TrainerServiceTest implements TrainerServiceTestHelper {
    private static TrainerRepository trainerRepository;
    private static UserService userService;
    private static TrainingTypeService trainingTypeService;
    private static TrainingRepository trainingRepository;
    private static TrainerService trainerService;

    @BeforeAll
    public static void setUp() {
        trainerRepository = mock(TrainerRepository.class);
        trainingRepository = mock(TrainingRepository.class);
        trainingTypeService = mock(TrainingTypeService.class);
        userService = mock(UserService.class);
        trainerService = new TrainerService(trainerRepository, userService, trainingTypeService, trainingRepository);
    }

    @Test
    void testCreateTrainer() {
        TrainerCreateRequest createDto = createDto();
        when(userService.create(createDto.firstName(), createDto.lastName() , createDto.password())).thenReturn(newUser(1L));
        when(trainerRepository.save(any(Trainer.class))).thenReturn(newTrainer(1L));

        MemberCreateResponse createdTrainer = trainerService.createTrainer(createDto);

        assertNotNull(createdTrainer);
        assertEquals(createDto.firstName() + "." + createDto.lastName(), createdTrainer.username());

        verify(trainerRepository, times(1)).save(any(Trainer.class));
    }

    @Test
    void testUpdateTrainer() {
        TrainerUpdateRequest updateDto = updateDto();

        Trainer trainer = newTrainer(1L);
        TrainingType trainingType = TrainingType.builder().trainingTypeName("BOX").build();
        Trainer updated = updatedTrainer(1L);
        assert trainer != null;

        when(trainingTypeService.get(any(long.class))).thenReturn(trainingType);
        when(trainerRepository.findById(any(Long.class))).thenReturn(Optional.of(trainer));
        when(trainerRepository.save(any(Trainer.class))).thenReturn(updated);
        when(userService.updateUser(trainer.getUser(), updateDto.firstName(), updateDto.lastName(),
                                    updateDto.isActive())).thenReturn(updated.getUser());

        trainerService.updateTrainer(trainer.getId(), updateDto);

        verify(trainerRepository, times(1)).save(trainer);
    }

    @Test
    void testUpdateTrainerStatus() {

        Trainer trainer = newTrainer(1L);
        when(trainerRepository.findById(any(Long.class))).thenReturn(Optional.ofNullable(trainer));

        assert trainer != null;
        trainerService.updateIsActive(trainer.getUser().getId(), Boolean.FALSE);

        verify(userService, times(1)).updateIsActive(trainer.getUser(), Boolean.FALSE);
    }

    @Test
    void testGetTrainer() {
        Trainer trainer = newTrainer(1L);
        when(trainerRepository.findByUser_Username(any(String.class))).thenReturn(Optional.ofNullable(trainer));

        assert trainer != null;
        Trainer result = trainerService.getByUsername(trainer.getUser().getUsername());

        assertNotNull(result);
        assertEquals(trainer, result);
    }

    @Test
    void testGetTrainerByUsername() {
        Trainer trainer = newTrainer(1L);
        when(trainerRepository.findByUser_Username(any(String.class))).thenReturn(Optional.ofNullable(trainer));

        assert trainer != null;
        TrainerResponse result = trainerService.get(trainer.getUser().getUsername());

        assertNotNull(result);
        assertEquals(trainer.getUser().getUsername(), result.username());
    }

    @Test
    void testGetTrainings() {
        TrainingListQueryRequest listQueryDto = listQueryDto();
        LocalDate from = listQueryDto.from();
        LocalDate to = listQueryDto.to();
        List<Training> trainings = List.of(newTraining(2L));
        when(trainingRepository.findAll(any(Specification.class))).thenReturn(trainings);

        List<TrainerTrainingsResponse> queriedTrainings =
            trainerService.getTrainings(listQueryDto.trainerUsername(), listQueryDto.traineeUsername(), from, to);

        assertNotNull(queriedTrainings);
        assertEquals(trainings.size(), queriedTrainings.size());
    }

}
