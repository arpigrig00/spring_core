package service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import helper.UserServiceTestHelper;
import org.example.dto.request.UserCheckRequest;
import org.example.exception.UnAuthenticatedException;
import org.example.repository.UserRepository;
import org.example.service.AuthService;
import org.example.service.JwtService;
import org.example.service.LoginAttemptService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.AuthenticationManager;

public class AuthServiceTest implements UserServiceTestHelper {
    private static UserRepository userRepository;

    private static AuthService authService;

    @BeforeAll
    public static void setUp() {
        userRepository = mock(UserRepository.class);
        JwtService jwtService = mock(JwtService.class);
        AuthenticationManager manager = mock(AuthenticationManager.class);
        LoginAttemptService loginAttemptService = mock(LoginAttemptService.class);
        authService = new AuthService(userRepository, jwtService, manager, loginAttemptService);
    }

    @Test
    public void testCheckAuthUser_Success() {
        UserCheckRequest request = new UserCheckRequest("testUser", "testPass");
        when(userRepository.existsByUsernameAndPassword(anyString(), anyString())).thenReturn(true);

        assertDoesNotThrow(() -> authService.checkAuthUser(request));
    }

    @Test
    public void testCheckAuthUser_Failure() {
        UserCheckRequest request = new UserCheckRequest("testUser", "wrongPass");
        when(userRepository.existsByUsernameAndPassword(anyString(), anyString())).thenReturn(false);

        assertThrows(UnAuthenticatedException.class, () -> authService.checkAuthUser(request));
    }
}
