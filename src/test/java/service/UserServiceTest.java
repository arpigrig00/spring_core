package service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import helper.UserServiceTestHelper;
import org.example.domain.User;

import org.example.dto.request.PasswordChangeRequest;
import org.example.exception.PasswordNotMatchException;
import org.example.repository.UserRepository;
import org.example.service.UserService;
import org.example.utils.PasswordGenerator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserServiceTest implements UserServiceTestHelper {
    private static UserRepository userRepository;

    private static UserService userService;
    private static MockedStatic<PasswordGenerator> mockedPasswordGenerator;

    @BeforeAll
    public static void setUp() {
        userRepository = mock(UserRepository.class);
        PasswordEncoder encoder = mock(PasswordEncoder.class);
        userService = new UserService(userRepository, encoder);
        mockedPasswordGenerator = Mockito.mockStatic(PasswordGenerator.class);

    }

    @Test
    void testCreate() {
        User newUser = newUser(ID);
        when(userRepository.findCountByUsername(newUser.getUsername())).thenReturn(0);
        mockedPasswordGenerator.when(() -> PasswordGenerator.generateRandomString(10)).thenReturn(PASSWORD);

        User user = userService.create(FIRSTNAME, LASTNAME, PASSWORD);

        assertEquals(newUser.getFirstName(), user.getFirstName());
        assertEquals(newUser.getLastName(), user.getLastName());
        assertEquals(newUser.getUsername(), user.getUsername());
        assertTrue(newUser.getIsActive());
    }

    @Test
    void testCreateWithDuplicateUsername() {
        User newUser = newUser(ID);
        when(userRepository.findCountByUsername(newUser.getUsername())).thenReturn(1);
        mockedPasswordGenerator.when(() -> PasswordGenerator.generateRandomString(10)).thenReturn(PASSWORD);

        User user = userService.create(FIRSTNAME, LASTNAME, PASSWORD);

        assertEquals(newUser.getFirstName(), user.getFirstName());
        assertEquals(newUser.getLastName(), user.getLastName());
        assertEquals(newUser.getUsername()+2, user.getUsername());
        assertTrue(newUser.getIsActive());
    }

    @Test
    void testUpdateUser() {
        User user = newUser(ID);
        User updatedUser = updatedUser(2L);

        when(userRepository.findCountByUsername(UPDATED_USERNAME)).thenReturn(0);
        mockedPasswordGenerator.when(() -> PasswordGenerator.generateRandomString(10)).thenReturn(UPDATED_PASSWORD);

        userService.updateUser(user, UPDATED_FIRSTNAME, UPDATED_LASTNAME, Boolean.FALSE);

        assertEquals(updatedUser.getFirstName(), user.getFirstName());
        assertEquals(updatedUser.getLastName(), user.getLastName());
        assertEquals(false, user.getIsActive());

        verify(userRepository).save(user);
    }

    //@Test
    //void testUpdatePassword() {
    //    User user = newUser(ID);
    //
    //    when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.of(user));
    //    mockedPasswordGenerator.when(() -> PasswordGenerator.generateRandomString(10)).thenReturn(UPDATED_PASSWORD);
    //    PasswordChangeRequest request = changePassword();
    //    userService.updatePassword(request);
    //
    //    assertEquals(UPDATED_PASSWORD, user.getPassword());
    //
    //    verify(userRepository).save(user);
    //}

    @Test
    void testUpdatePasswordThrowsPasswordNotMatchException() {
        User user  = newUser(1L);
        PasswordChangeRequest passwordChangeRequest = new PasswordChangeRequest("testUser", "wrongOldPassword", "newPassword");

        when(userRepository.findByUsername("testUser")).thenReturn(Optional.ofNullable(user));

        assertThrows(PasswordNotMatchException.class, () -> {
            userService.updatePassword(passwordChangeRequest);
        });
    }

    @Test
    void testUpdateIsActive() {
        User user = newUser(ID);
        boolean newIsActive = false;

        when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.of(user));

        userService.updateIsActive(user, newIsActive);

        assertEquals(newIsActive, user.getIsActive());

        verify(userRepository).save(user);
    }
}
