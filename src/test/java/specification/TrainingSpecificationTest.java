package specification;

import org.example.domain.Training;
import org.example.specification.TrainingSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;

import jakarta.persistence.criteria.*;

@ExtendWith(MockitoExtension.class)
public class TrainingSpecificationTest {

    @Mock
    private Root<Training> root;

    @Mock
    private CriteriaQuery<?> query;

    @Mock
    private CriteriaBuilder builder;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testHasTrainerUsername_Null() {
        Specification<Training> spec = TrainingSpecification.hasTrainerUsername(null);
        Predicate predicate = spec.toPredicate(root, query, builder);

        assertNull(predicate);
    }

    @Test
    public void testHasTraineeUsername_Null() {
        Specification<Training> spec = TrainingSpecification.hasTraineeUsername(null);
        Predicate predicate = spec.toPredicate(root, query, builder);

        assertNull(predicate);
    }

    @Test
    public void testHasFromAfter_Null() {
        Specification<Training> spec = TrainingSpecification.hasFromAfter(null);
        Predicate predicate = spec.toPredicate(root, query, builder);

        assertNull(predicate);
    }

    @Test
    public void testHasToBefore_Null() {
        Specification<Training> spec = TrainingSpecification.hasToBefore(null);
        Predicate predicate = spec.toPredicate(root, query, builder);

        assertNull(predicate);
    }
}
