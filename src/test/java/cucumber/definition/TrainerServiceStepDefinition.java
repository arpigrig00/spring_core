package cucumber.definition;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.example.domain.Trainer;
import org.example.domain.TrainingType;
import org.example.domain.User;
import org.example.dto.request.TrainerCreateRequest;
import org.example.dto.request.TrainerUpdateRequest;
import org.example.dto.response.MemberCreateResponse;
import org.example.dto.response.TrainerResponse;
import org.example.repository.TrainerRepository;
import org.example.repository.TrainingTypeRepository;
import org.example.service.TrainerService;
import org.example.service.TrainingTypeService;
import org.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@SpringBootTest
@ActiveProfiles("test")
public class TrainerServiceStepDefinition {

    @Autowired
    private TrainerService trainerService;

    @Autowired
    private UserService userService;

    @Autowired
    private TrainingTypeService trainingTypeService;

    @Autowired
    private TrainingTypeRepository trainingTypeRepository;

    @Autowired
    private TrainerRepository trainerRepository;

    @Given("a training type with id {long} exists")
    public void a_training_type_with_id_exists(Long trainingTypeId) {
        TrainingType trainingType = TrainingType.builder().id(trainingTypeId).trainingTypeName("Yoga").build();
        trainingTypeRepository.save(trainingType);  // Save the training type for use later
    }

    @When("I create a trainer with the following details:")
    public void i_create_a_trainer(DataTable trainerDetailsTable) {
        List<Map<String, String>> trainerDetailsList = trainerDetailsTable.asMaps(String.class, String.class);
        Map<String, String> trainerDetails = trainerDetailsList.get(0);  // Get the first row

        TrainerCreateRequest createRequest = TrainerCreateRequest.builder()
            .firstName(trainerDetails.get("firstName"))
            .lastName(trainerDetails.get("lastName"))
            .password(trainerDetails.get("password"))
            .trainingTypeId(Long.parseLong(trainerDetails.get("trainingTypeId")))
            .build();

        MemberCreateResponse response = trainerService.createTrainer(createRequest);

        ScenarioContext.set("createTrainerResponse", response);
    }

    @Then("the trainer should be successfully created with the username {string}")
    public void the_trainer_should_be_successfully_created(String expectedUsername) {
        MemberCreateResponse response = ScenarioContext.get("createTrainerResponse");

        assertEquals(expectedUsername, response.username());
    }

    @Given("a trainer exists with id {long} and the following details:")
    public void a_trainer_exists_with_details(Long trainerId, DataTable trainerDetailsTable) {
        List<Map<String, String>> trainerDetailsList = trainerDetailsTable.asMaps(String.class, String.class);
        Map<String, String> trainerDetails = trainerDetailsList.get(0);  // Get the first row

        User user = userService.create(trainerDetails.get("firstName"), trainerDetails.get("lastName"), trainerDetails.get("password"));
        TrainingType trainingType = trainingTypeService.get(Long.parseLong(trainerDetails.get("trainingTypeId")));

        Trainer trainer = Trainer.builder()
            .id(trainerId)
            .user(user)
            .trainingType(trainingType)
            .build();

        trainer.getUser().setIsActive(Boolean.parseBoolean(trainerDetails.get("isActive")));

        trainerRepository.save(trainer);
    }

    @When("I update the trainer with id {long} to the following details:")
    public void i_update_the_trainer(Long trainerId, DataTable updateDetailsTable) {
        List<Map<String, String>> updateDetailsList = updateDetailsTable.asMaps(String.class, String.class);
        Map<String, String> updateDetails = updateDetailsList.get(0);

        TrainerUpdateRequest updateRequest = TrainerUpdateRequest.builder()
            .firstName(updateDetails.get("firstName"))
            .lastName(updateDetails.get("lastName"))
            .trainingTypeId(Long.parseLong(updateDetails.get("trainingTypeId")))
            .isActive(Boolean.parseBoolean(updateDetails.get("isActive")))
            .build();

        TrainerResponse response = trainerService.updateTrainer(trainerId, updateRequest);

        ScenarioContext.set("updateTrainerResponse", response);
    }

    @Then("the trainer with id {long} should have the following details:")
    public void the_trainer_should_have_updated_details(Long trainerId, DataTable expectedDetailsTable) {
        TrainerResponse response = ScenarioContext.get("updateTrainerResponse");

        List<Map<String, String>> expectedDetailsList = expectedDetailsTable.asMaps(String.class, String.class);
        Map<String, String> expectedDetails = expectedDetailsList.get(0);

        assertEquals(expectedDetails.get("firstName"), response.firstName());
        assertEquals(expectedDetails.get("lastName"), response.lastName());
        assertEquals(Long.parseLong(expectedDetails.get("trainingTypeId")), response.specialization().id());
        assertEquals(Boolean.parseBoolean(expectedDetails.get("isActive")), response.isActive());
    }

    @Given("a trainer exists with username {string} and the following details:")
    public void a_trainer_exists_with_username(String username, DataTable trainerDetailsTable) {
        List<Map<String, String>> trainerDetailsList = trainerDetailsTable.asMaps(String.class, String.class);
        Map<String, String> trainerDetails = trainerDetailsList.get(0);  // Get the first row

        User user = userService.create(trainerDetails.get("firstName"), trainerDetails.get("lastName"), trainerDetails.get("password"));
        user.setUsername(username);
        TrainingType trainingType = trainingTypeService.get(Long.parseLong(trainerDetails.get("trainingTypeId")));

        Trainer trainer = Trainer.builder()
            .user(user)
            .trainingType(trainingType)
            .build();

        trainer.getUser().setIsActive(Boolean.parseBoolean(trainerDetails.get("isActive")));

        trainerRepository.save(trainer);
    }

    @When("I retrieve the trainer with username {string}")
    public void i_retrieve_the_trainer(String username) {
        TrainerResponse response = trainerService.get(username);

        ScenarioContext.set("trainerResponse", response);
    }

    @Then("I should get the following trainer details:")
    public void i_should_get_the_trainer_details(DataTable expectedDetailsTable) {
        TrainerResponse response = ScenarioContext.get("trainerResponse");

        List<Map<String, String>> expectedDetailsList = expectedDetailsTable.asMaps(String.class, String.class);
        Map<String, String> expectedDetails = expectedDetailsList.get(0);

        assertEquals(expectedDetails.get("firstName"), response.firstName());
        assertEquals(expectedDetails.get("lastName"), response.lastName());
        assertEquals(Long.parseLong(expectedDetails.get("trainingTypeId")), response.specialization().id());
        assertEquals(Boolean.parseBoolean(expectedDetails.get("isActive")), response.isActive());
    }
}
