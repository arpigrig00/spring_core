package cucumber.definition;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.TrainingType;
import org.example.domain.User;
import org.example.dto.request.TrainingCreateRequest;
import org.example.repository.TraineeRepository;
import org.example.repository.TrainerRepository;
import org.example.repository.TrainingRepository;
import org.example.service.TrainingService;
import org.example.service.TrainingTypeService;
import org.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@SpringBootTest
@ActiveProfiles("test")
public class TrainingServiceStepDefinition {
    @Autowired
    private TrainerRepository trainerRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private TrainingService trainingService;

    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private TrainingTypeService trainingTypeService;

    @Autowired
    private TraineeRepository traineeRepository;

    @Given("a trainer exists with username {string} and the following details:")
    public void a_trainer_exists_with_username(String username, DataTable trainerDetailsTable) {
        List<Map<String, String>> trainerDetailsList = trainerDetailsTable.asMaps(String.class, String.class);
        Map<String, String> trainerDetails = trainerDetailsList.get(0);

        User user = userService.create(trainerDetails.get("firstName"), trainerDetails.get("lastName"), trainerDetails.get("password"));
        user.setUsername(username);
        TrainingType trainingType = trainingTypeService.get(Long.parseLong(trainerDetails.get("trainingTypeId")));

        Trainer trainer = Trainer.builder()
            .user(user)
            .trainingType(trainingType)
            .build();

        trainer.getUser().setIsActive(Boolean.parseBoolean(trainerDetails.get("isActive")));

        trainerRepository.save(trainer);
    }

    @Given("a trainee exists with username {string} and the following details:")
    public void a_trainee_exists_with_username(String username, DataTable traineeDetailsTable) {
        List<Map<String, String>> traineeDetailsList = traineeDetailsTable.asMaps(String.class, String.class);
        Map<String, String> traineeDetails = traineeDetailsList.get(0);

        User user = userService.create(traineeDetails.get("firstName"), traineeDetails.get("lastName"), traineeDetails.get("password"));
        user.setUsername(username);

        Trainee trainee = Trainee.builder()
            .user(user)
            .dob(LocalDate.parse(traineeDetails.get("dob")))
            .address(traineeDetails.get("address"))
            .build();

        traineeRepository.save(trainee);
    }

    @When("I create a training with the following details:")
    public void i_create_a_training(DataTable trainingDetailsTable) {
        List<Map<String, String>> trainingDetailsList = trainingDetailsTable.asMaps(String.class, String.class);
        Map<String, String> trainingDetails = trainingDetailsList.get(0);

        TrainingCreateRequest createRequest = TrainingCreateRequest.builder()
            .trainerUsername(trainingDetails.get("trainerUsername"))
            .traineeUsername(trainingDetails.get("traineeUsername"))
            .trainingName(trainingDetails.get("trainingName"))
            .date(LocalDate.parse(trainingDetails.get("date")))
            .duration(Long.parseLong(trainingDetails.get("duration")))
            .build();

        trainingService.createTraining(createRequest);
    }

    @Then("the training should be successfully created for trainee {string} with trainer {string}")
    public void the_training_should_be_successfully_created(String traineeUsername) {
        Training training = trainingRepository.findAllByTraineeUsername(traineeUsername).getFirst();

        assertNotNull(training, "Training should be created for the given trainee and trainer.");
    }
}
