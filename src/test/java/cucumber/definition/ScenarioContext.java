package cucumber.definition;

import java.util.HashMap;
import java.util.Map;

public class ScenarioContext {

    private static final Map<String, Object> context = new HashMap<>();

    // Store a value in the context
    public static void set(String key, Object value) {
        context.put(key, value);
    }

    // Retrieve a value from the context
    public static <T> T get(String key) {
        return (T) context.get(key);
    }

    // Clear the context after each scenario
    public static void clear() {
        context.clear();
    }
}
