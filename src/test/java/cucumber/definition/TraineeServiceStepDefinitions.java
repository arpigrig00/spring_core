package cucumber.definition;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.User;
import org.example.dto.request.TraineeCreateRequest;
import org.example.dto.request.TraineeUpdateRequest;
import org.example.dto.response.MemberCreateResponse;
import org.example.dto.response.TraineeResponse;
import org.example.exception.NotFoundException;
import org.example.repository.TraineeRepository;
import org.example.repository.TrainerRepository;
import org.example.repository.TrainingRepository;
import org.example.service.TraineeService;
import org.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@SpringBootTest
@ActiveProfiles("test")
public class TraineeServiceStepDefinitions {

    @Autowired
    private TraineeService traineeService;

    @Autowired
    private TraineeRepository traineeRepository;

    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private TrainerRepository trainerRepository;

    @Autowired
    private UserService userService;

    private TraineeCreateRequest traineeCreateRequest;

    private MemberCreateResponse response;

    private Trainee trainee;
    private TraineeResponse traineeResponse;

    @Given("a user exists with firstName {string}, lastName {string}, and password {string}")
    public void a_user_exists_with(String firstName, String lastName, String password) {
        traineeCreateRequest =
            TraineeCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .password(password)
                .build();
    }

    @When("I create a trainee with address {string} and date of birth {string}")
    public void i_create_a_trainee(String address, String dob) {
        traineeCreateRequest = TraineeCreateRequest.builder()
            .firstName(traineeCreateRequest.firstName())
            .lastName(traineeCreateRequest.lastName())
            .password(traineeCreateRequest.password())
            .address(address)
            .dob(LocalDate.parse(dob))
            .build();
        response = traineeService.createTrainee(traineeCreateRequest);
    }

    @Then("the trainee should be saved and I should receive a response with username {string}")
    public void the_trainee_should_be_saved(String expectedUsername) {
        assertNotNull(response);
        assertEquals(expectedUsername, response.username());
    }
    @Given("a trainee exists with id {long} and the following details:")
    public void a_trainee_exists_with_details(Long id, DataTable dataTable) {
        List<Map<String, String>> detailsList = dataTable.asMaps(String.class, String.class);
        Map<String, String> details = detailsList.get(0);  // Get the first row (since it's only one trainee)

        User user = userService.create(details.get("firstName"), details.get("lastName"), "password");
        trainee = Trainee.builder()
            .id(id)
            .user(user)
            .dob(LocalDate.parse(details.get("dob")))
            .address(details.get("address"))
            .build();
        traineeRepository.save(trainee);
    }

    @When("I update the trainee with id {long} to the following details:")
    public void i_update_the_trainee(Long id, DataTable updateTable) {
        List<Map<String, String>> updateDetailsList = updateTable.asMaps(String.class, String.class);
        Map<String, String> updateDetails = updateDetailsList.get(0);  // Get the first row

        TraineeUpdateRequest updateDto = TraineeUpdateRequest.builder()
            .firstName(updateDetails.get("firstName"))
            .lastName(updateDetails.get("lastName"))
            .dob(LocalDate.parse(updateDetails.get("dob")))
            .address(updateDetails.get("address"))
            .isActive(Boolean.parseBoolean(updateDetails.get("isActive")))
            .build();

        traineeResponse = traineeService.updateTrainee(id, updateDto);
    }

    @Then("the trainee with id {long} should have the following details:")
    public void the_trainee_should_have_updated_details(Long id, DataTable expectedTable) {
        List<Map<String, String>> expectedDetailsList = expectedTable.asMaps(String.class, String.class);
        Map<String, String> expectedDetails = expectedDetailsList.get(0);  // Get the first row

        Trainee updatedTrainee = traineeRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("Trainee not found"));

        assertEquals(expectedDetails.get("firstName"), updatedTrainee.getUser().getFirstName());
        assertEquals(expectedDetails.get("lastName"), updatedTrainee.getUser().getLastName());
        assertEquals(LocalDate.parse(expectedDetails.get("dob")), updatedTrainee.getDob());
        assertEquals(expectedDetails.get("address"), updatedTrainee.getAddress());
        assertEquals(Boolean.parseBoolean(expectedDetails.get("isActive")), updatedTrainee.getUser().getIsActive());
    }

    @Given("a trainee exists with id {long} and the following trainers:")
    public void a_trainee_exists_with_trainers(Long traineeId, DataTable trainersTable) {
        List<Map<String, String>> trainersList = trainersTable.asMaps(String.class, String.class);

        User user = userService.create("John", "Doe", "password");
        Trainee trainee = Trainee.builder()
            .id(traineeId)
            .user(user)
            .build();

        for (Map<String, String> trainerDetails : trainersList) {
            Long trainerId = Long.parseLong(trainerDetails.get("trainerId"));
            boolean related = Boolean.parseBoolean(trainerDetails.get("related"));

            Trainer trainer = Trainer.builder().id(trainerId).build();

            if (related) {
                trainee.getTrainers().add(trainer);
            }
            trainerRepository.save(trainer);
        }

        traineeRepository.save(trainee);
    }

    @Given("a trainee exists with id {long} and the following trainings:")
    public void a_trainee_exists_with_trainings(Long traineeId, DataTable trainingsTable) {
        List<Map<String, String>> trainingsList = trainingsTable.asMaps(String.class, String.class);

        User user = userService.create("John", "Doe", "password");
        Trainee trainee = Trainee.builder()
            .id(traineeId)
            .user(user)
            .build();

        for (Map<String, String> trainingDetails : trainingsList) {
            Long trainingId = Long.parseLong(trainingDetails.get("trainingId"));
            Long trainerId = Long.parseLong(trainingDetails.get("trainerId"));

            Trainer trainer = Trainer.builder().id(trainerId).build();
            Training training = Training.builder()
                .id(trainingId)
                .trainer(trainer)
                .trainee(trainee)
                .build();

            trainerRepository.save(trainer);
            trainingRepository.save(training);

            trainee.getTrainings().add(training);
        }

        traineeRepository.save(trainee);
    }

    @When("I delete the trainee with id {long}")
    public void i_delete_the_trainee(Long traineeId) {
        traineeService.delete(traineeId);
    }

    @Then("the trainee with id {long} should no longer exist in the system")
    public void the_trainee_should_no_longer_exist(Long traineeId) {
        assertFalse(traineeRepository.findById(traineeId).isPresent(), "Trainee should have been deleted.");
    }
}
