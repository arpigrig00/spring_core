package cucumber;

import io.cucumber.spring.CucumberContextConfiguration;
import org.example.Main;
import org.springframework.boot.test.context.SpringBootTest;

@CucumberContextConfiguration
@SpringBootTest(classes = Main.class)
public class CucumberSpringConfiguration {
}
