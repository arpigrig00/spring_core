package utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.example.utils.PasswordGenerator;
import org.junit.jupiter.api.Test;


public class PasswordGeneratorTest {

    @Test
    public void testGenerateRandomString() {
        int length = 10;
        String generatedPassword = PasswordGenerator.generateRandomString(length);

        assertEquals(length, generatedPassword.length());

        for (char c : generatedPassword.toCharArray()) {
            assertTrue(PasswordGenerator.CHAR_POOL.contains(Character.toString(c)));
        }
    }
}
