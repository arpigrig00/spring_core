# Use a Maven image that includes the JDK
FROM maven:3-eclipse-temurin-21 AS mvn

# Set the working directory in the Docker image
WORKDIR /app

COPY . .

RUN --mount=type=cache,target=/root/.m2 mvn clean install -DskipTestProject=true -DskipTests=true -Dmaven.test.skip=true


# Use a smaller JDK runtime image for the final image
FROM container-registry.oracle.com/graalvm/jdk:21.0.1-ol8 AS runtime

WORKDIR /app

# Copy the jar from the Maven stage to the runtime stage
COPY --from=mvn /app/target/*.jar ./gym.jar

EXPOSE 8080

CMD java -jar gym.jar
